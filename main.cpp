/**
 *\mainpage
 *\par Gra turowa RPG
 * Program w którym użytkownik może wcielić sie w postać bohatera, walczyć z potwtorami, zdobywać umiejętnośći
 * \author Piotr Grygo
 * \version 1.0
 * \par Kontakt:
 * \a piotr.grygo94@gmail.com
 */


#include <iostream>
#include "funkcje.h"
using namespace std;
#include <string>
#include <cstdlib>
#include <ctime>
#include <fstream>

int main()
{


     // wstęp do gry dotyczący zasad działania gry oraz silnika gry

    cout<<"Witaj w grze ;)"<<'\n'<<"w grze poruszasz sie za pomoca klawiszy,"<<'\n'<<"kiedy w menu pojawiaja sie liczby 1,2,3... wybierasz swoj ruch po przez wcisniecie odpowiedniej liczby";
    cout<<'\n'<<"gry pojawi sie wybot t/n musisz wcisnac odpowiednia <t> jezeli chcesz wykonac propozycje lub <n> jezeli nie"<<'\n';
    cout<<"jezeli nie ma ani wyboru t/n ani liczb musisz wpiasc nazwe ktora wybierasz"<<'\n'<<'\n';
    cout<<"zycze mnostwa wspanialych przezyc z gra ;)"<<'\n'<<'\n';
          system("PAUSE");
          system ("cls");

    cout<<"początkowo nie posiadasz zadnych przedmiotow ani umijetnosci"<<"\n";
    cout<<"przedmioty zdobywasz po pokaniuniu kazdego przeciwnkia"<<'\n';
    cout<<"umiejstnosci zdobywasz wraz ze zdobyciem levelu"<<'\n';
    system("PAUSE");
    system ("cls");
//wczytywanie postaci/tworzenie
    postac Bohater;
    int wyborop;
    string nick;

//wyswietlenie wyboru opcji

    cout<<"wybor opcji"<<'\n'<<'\n'<<"1-stworz nowa postac"<<'\n'<<"2-wczytaj postac"<<'\n'<<"3-wyjdz"<<'\n'<<'\n';
    cin>>wyborop;

///wybor 1, tworzenie nowej postaci

    if(wyborop==1){
        cout<<"podaj nick"<<endl<<endl;
        cin>>nick;
        Bohater=tworzeniepostaci(nick);
        cout<<"stworzyles nowa postac"<<endl;
    }

///wybor 2,wczytanie juz zapisanej postaci
    else if(wyborop==2){
        cout<<"podaj nick istniejace postaci"<<endl<<endl;
        cin>>nick;
       Bohater=wczytajpostac(nick);
        cout<<"pomyslnie wczytales postac"<<endl;
    }
///wyjscie z gry

    else{
        return 0;
    }

 ///koniec wczytywania,





    ///wybor lokacji

    srand (time(0));
    string wyborlok;
    string  wyborloks;
    int wyborakc;
    int los;
    string skarb;
    string opis;
 wybor_lokacji:
    cout<<"wybierz lokacje:"<<endl<<"miasto"<<endl<<"ruiny"<<endl<<"las"<<endl<<"bagno"<<endl<<endl;
    cin>>wyborloks;
    wyborlok= "lok_"+wyborloks+".txt";
    ifstream ilokplik;
    ilokplik.open(wyborlok.c_str());
    getline(ilokplik, opis);
    getline(ilokplik, opis);
    cout<<opis<<endl<<endl;
 wybor_dzialania:

    cout<<"wybierz akcje:"<<endl<<"1. szukaj guza"<<endl<<"2. szukaj skarbow"<<endl<<"3. zmien lokacje"<<endl<<"4. zobcz karte postaci"<<endl<<"5. zapisz"<<endl<<"6. zakoncz"<<endl<<endl;
    cin>>wyborakc;

    switch(wyborakc){
    case 1:
        ilokplik.close();
      Bohater=walka(Bohater, wyborlok);
         ilokplik.open(wyborlok.c_str());
         if(Bohater.hp<=0){
             return 0;
         }
         system("cls");
         cout<<"jestes w "<<wyborloks<<endl;
         goto wybor_dzialania;

    case 2: //szukanie skarbow, losowanie liczby, jezeli 5 to skarb znaleziony
       los=1+9*rand()/double(RAND_MAX);

       if(los==5){
          do{
           getline(ilokplik, skarb);
           }while(skarb!="kryjowki");
           getline(ilokplik, opis);
               cout<<opis<<endl;
        }
      else {
        cout<<"niestety nic nie znalzles :( "<<endl;
       }

      system("PAUSE");


      system("cls");
     cout<<"co teraz?"<<endl;
      goto wybor_dzialania;


    case 3:
        ilokplik.close();
        system ("cls");
      goto wybor_lokacji;
   case 4:
       system ("cls");
        wyswietlpostac(Bohater);
        system("PAUSE");



       goto wybor_dzialania;
    case 5:
        Zapisz(Bohater);
        system("cls");
      cout<<"zapis pomyslny"<<endl;
      goto wybor_dzialania;


    case 6:
        return 0;

    }

    return 0;
}

