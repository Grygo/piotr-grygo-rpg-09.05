#include "funkcje.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>

/**
 * @brief tworzeniepostaci
* funcka tworząca nową postać i zapisująca ją do odpowiedniej nowej nawzwie
* wczytuje również funkcję do sturktury postaci ktora bedzie wykorzystywana w dalszej grze
 * @param nick
 * @return zwraca nową strukturę postaci
 

*/
postac tworzeniepostaci(string nick){
    postac Bohater;
    Bohater.nazwa=nick;
    ofstream oplik;
    Bohater.p1="0";
    Bohater.p2="0";
    Bohater.p3="0";
    Bohater.u1="0";
    Bohater.u2="0";
    Bohater.u3="0";
    string plik="post_"+nick+".txt";
    oplik.open(plik.c_str());

    Bohater.hp=15; Bohater.mana=10; Bohater.exp=0; Bohater.atak=5;Bohater.obrona=5;Bohater.lvl=1;Bohater.gold=0;

    oplik<<Bohater.nazwa<<'\n'<< Bohater.hp<<'\n'<<Bohater.mana<<'\n'<<Bohater.atak<<'\n'<<Bohater.obrona<<'\n'<<Bohater.exp<<'\n'<<Bohater.lvl<<'\n'<<Bohater.gold<<'\n';
    oplik<<"umiejetnosci: "<<'\n'<< Bohater.u1<<'\n'<< Bohater.u2<<'\n'<< Bohater.u3<<'\n';

   oplik<<"przedmioty: "<<'\n'<<Bohater.p1<<'\n'<<Bohater.p2<<'\n'<<Bohater.p3<<'\n';
          oplik.close();



return Bohater;
}
/**
 * @brief wczytajpostac
 * fukncja wczytująca kartę postaci z pliku do struktry wykorzystywanej w daleszej grze
 * użytkownik podaje nazwę gracza, która zostaje przerobiona i otwiera odpowiedni plik z kartą postaci
 * @param nick
 * @return zwraca wczytaną strukturę postaci
 */
postac wczytajpostac (string nick){
    postac Bohater;
    ifstream ibohplik;
    string pusty;
    string plik="post_"+nick+".txt";

    ibohplik.open(plik.c_str());
    if(!ibohplik.is_open()){
        cout<<"nie ma takiej postaci";
        exit(0);
    }
    ibohplik>>Bohater.nazwa>>Bohater.hp>>Bohater.mana>>Bohater.atak>>Bohater.obrona>>Bohater.exp>>Bohater.lvl>>Bohater.gold;
    ibohplik>>pusty;
    ibohplik>>Bohater.u1>>Bohater.u2>>Bohater.u3;
    ibohplik>>pusty;
    ibohplik>>Bohater.p1>>Bohater.p2>>Bohater.p3;






    ibohplik.close();
    return Bohater;
}
/**
 * @brief Zapisz
 * funkcja zapisująca statystki, ekwipunek, umiejetsnosci do pliku bedace karta postaci
 * @param Bohater
 */
void Zapisz(postac Bohater){

    ofstream oplik;
    string plik="post_"+Bohater.nazwa+".txt";
    oplik.open(plik.c_str());
    oplik<<Bohater.nazwa<<'\n'<< Bohater.hp<<'\n'<<Bohater.mana<<'\n'<<Bohater.atak<<'\n'<<Bohater.obrona<<'\n'<<Bohater.exp<<'\n'<<Bohater.lvl<<'\n'<<Bohater.gold;
    oplik<<"umiejetnosci: "<<'\n';
    oplik<<Bohater.u1<<'\n'<<Bohater.u2<<'\n'<<Bohater.u3<<'\n';
    oplik<<"przedmioty: "<<'\n';
    oplik<<Bohater.p1<<'\n'<<Bohater.p2<<'\n'<<Bohater.p3<<'\n';
    oplik.close();

}
/**
   * @brief MenuStartowe
   * funkcja otwierajaca menu startowe z mozliwoscia wyboru opcji
 */
void MenuStartowe(){
    int wyborop;
    string nick;
    cout<<"wybór opcji"<<'\n'<<'\n'<<"1-stwórz nową postać"<<'\n'<<"2-wczytaj postać"<<'\n'<<"3-wyjdź";
    cin>>wyborop;
    if(wyborop==1){
        cout<<"podaj nick";
        cin>>nick;
        tworzeniepostaci(nick);

    }
    else if(wyborop==2){
        cout<<"podaj nick";
        cin>>nick;
        wczytajpostac(nick);

    }


}
void gra(postac Bohater){
   srand (time(0));
   string wyborlok;
   int wyborakc;
   int los;
   string skarb;
   string opis;
wybor_lokacji:
   cout<<"wybierz lokacje:"<<endl<<"miasto"<<endl<<"ruiny"<<endl<<"las"<<endl<<"bango"<<endl<<endl;
   cin>>wyborlok;
   wyborlok= "lok_"+wyborlok+".txt";
   ifstream ilokplik;
   ilokplik.open(wyborlok.c_str());
   getline(ilokplik, opis);
   getline(ilokplik, opis);
   cout<<opis<<endl<<endl;
wybor_dzialania:
   cout<<"wybierz akcje:"<<endl<<"1. szukaj guza"<<endl<<"2. szukaj skarbów"<<endl<<"3.zmień lokacje"<<endl<<"4. zapisz"<<endl<<endl;
   cin>>wyborakc;
   switch(wyborakc){
   case 1: cout<<"póki co jeszcze nic"<<endl;
       cout<<"co teraz?"<<endl;
        goto wybor_dzialania;
   case 2: //szukanie skarbow, losowanie liczby, jezeli 50 to skarb znaleziony
      los=1+99*rand()/double(RAND_MAX);

      if(los==50){
         do{
          getline(ilokplik, skarb);
          }while(skarb!="kryjowki");
          getline(ilokplik, opis);
              cout<<opis<<endl;
              //dokończyćasdnhsiahnfjkenjkwenfgejkwngfjkwngfjkwengfjkngj!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       }
     else {
       cout<<"niestety nic nie znalzłeś :( "<<endl;
      }

     cout<<"co teraz?"<<endl;
     goto wybor_dzialania;


   case 3:
     goto wybor_lokacji;
   case 4:
       Zapisz(Bohater);
       cout<<"zapis pomyslny"<<endl;
       goto wybor_dzialania;
    }
   }
/**
 * @brief wyswietlpostac
 * funkca wyswietlajca graczowi karte postaci bohatera
 * @param Bohater
 */
void wyswietlpostac(postac Bohater){
    cout<<"nick "<<Bohater.nazwa<<endl;
    cout<<"hp "<<Bohater.hp<<'\n';
    cout<<"mana "<<Bohater.mana<<'\n';
    cout<<"atak "<<Bohater.atak<<'\n';
    cout<<"obrona "<<Bohater.obrona<<'\n';
    cout<<"exp "<<Bohater.exp<<'\n';
    cout<<"level "<<Bohater.lvl<<'\n';
    cout<<"zloto "<<Bohater.gold<<'\n';
    cout<<"umiejetnosci:"<<'\n';
    cout<<Bohater.u1<<'\n'<<Bohater.u2<<'\n'<<Bohater.u3<<'\n';
    cout<<"przedmioty:"<<'\n';
    cout<<Bohater.p1<<'\n'<<Bohater.p2<<'\n'<<Bohater.p3<<'\n';
}
/**
 * @brief nowyprzedmiot
 * funkcja zamieniajaca przedmiot w ekwipunku bohatera na nowy wyolosywany z pokanengo przeciwnika
 * @param Boh
 * @param nazwa
 * @return strukturę postaci z nowym przedmiotem
 */
postac nowyprzedmiot(postac Boh, string nazwa){
    string wyb;
    int wybp;

    cout<<"jestes aktualnie w posiadaniu: "<<endl;
    cout<<"1. "<< Boh.p1<<'\n'<<"2. "<<Boh.p2<<'\n'<<"3. "<<Boh.p3<<'\n'<<'\n';
    cout<<"czy chcesz zamienic ktorys pzedmiot? t/n"<<'\n';
   cin>>wyb;
    if(wyb=="t"){
        cout<<"wybierz numer przedmiotu "<<'\n';
        cin>>wybp;
   switch (wybp){

   case 1:
       Boh.p1=nazwa;

       break ;
   case 2:
       Boh.p2=nazwa;
       break ;
   case 3:
       Boh.p3=nazwa;
       break ;


        }
     }

    return Boh;
}
/**
 * @brief nowaumiejetnosc
 * funkcja ktora umozliwia zdobycie/wymiane nowej umijetnosci po uzyskaniu przez bohatera levelu
 * @param boh
 * @return strukturę postaci z nową umiejętnością
 */
postac nowaumiejetnosc (postac boh){
    char wybr='c';
    string wybrana;
    int wyb;
    cout<<"posiadasz nastepujace umiejetnosci: "<<'\n'<<"1 " <<boh.u1<<'\n'<<"2 "<<boh.u2<<'\n'<<"3 "<<boh.u3<<'\n';
    while((wybr!='t')&&(wybr!='n')){
    cout<<"czy chcesz zamienic posiadana umiejtnosc? t/n"<<'\n';
    cin>>wybr;
    if(wybr=='t'){
    cout<<"wybierz nowa umiejetnosc"<<'\n';
    cout<<"wzmocnienie"<<'\n'<<"ognista_kula"<<'\n'<<"mocny_atak"<<'\n'<<"leczenie"<<'\n'<<"sredni_atak"<<'\n';
    cin>>wybrana;
    cout<<"wybierz umiejetnosc do zamiany"<<'\n';
    cin>>wyb;
    switch (wyb){
    case 1:
        boh.u1=wybrana;
        break;
    case 2:
        boh.u2=wybrana;
        break;
    case 3:
        boh.u3=wybrana;
        break;

    }
}



}
    return boh;
}
/**
 * @brief levelup
 * Funkcja sprawdzajaca czy bohater ma wystaraczajaco duzo doswiadczenia, aby zdobyć kolejny level
 * jeżeli ma odpwiednią liczbę punktów doświadczenai zysukuje level
 * @param Bohater
 * \see nowaumiejetnosc(postac boh)
 * @return Bohater z levelem podniesionym
 */
postac levelup(postac Bohater){
    int pomoc=((Bohater.lvl+1)*(Bohater.lvl+1))*5;
    if(Bohater.exp>=pomoc){
        Bohater.lvl++;
        system("cls");
        cout<<endl<<"zysukujesz nowy level"<<endl<<"masz "<<Bohater.lvl<<"level"<<endl;
    Bohater.hp=Bohater.lvl*15;
    Bohater.mana=Bohater.lvl*16;
    Bohater.atak++;
    Bohater.obrona++;
    Bohater=nowaumiejetnosc(Bohater);
    }
    return Bohater;
}
/**
 * @brief losujprzedmiot
 * funkcja ktora losuje przedmiot po pokonaniu przeciwnika
 * @param nazwapotwora
 * @param Boh
 * \see nowyprzedmiot(postac Boh, string nazwa)
 * @return strukturę boahtera z nowym przdmiotem
 */
postac losujprzedmiot(string nazwapotwora, postac Boh){
    srand (time(0));
    ifstream ipotwplik;
    int los;
    string nazwaprzedmiotu;
    nazwapotwora="potw_"+nazwapotwora+".txt";
    ipotwplik.open(nazwapotwora.c_str());
    do {
        getline(ipotwplik, nazwaprzedmiotu);
    }while(nazwaprzedmiotu!="przedmiot");
    los=rand()%(3);
   ;
    for(int i=0; i<los+1; i++){
         getline(ipotwplik, nazwaprzedmiotu);

     }
 cout<<"W walce wygrales: "+nazwaprzedmiotu<<endl<<'\n';
if(nazwaprzedmiotu!="0"){
Boh=nowyprzedmiot(Boh, nazwaprzedmiotu);
}
else cout<<"niestety nic nie wygrales";
return Boh;




}
/**
 * @brief wybierzprzedmiot
 * funkcja pozwalajca na wybranie przedmiotu podczas walki 
 * @param Boh
 * @return strukturę przedmiotu która zostanei wykorzystana w walce
 */
przedmiot wybierzprzedmiot(postac &Boh){
    przedmiot wybrany;
    string plik;
    int wyb;
    ifstream ipmplik;
    bool czy=0;

    cout<<"wybierz umiejetnosc: "<<endl;
    cout<<"1 "<<Boh.p1<<endl;
    cout<<"2 "<<Boh.p2<<endl;
    cout<<"3 "<<Boh.p3<<endl;
    cout<<"4. nic jednak nie wybierasz"<<'\n';
    cin>>wyb;

    switch(wyb){
    case 1:
        wybrany.nazwa=Boh.p1;
        Boh.p1="0";
        czy=1;
        break;
    case 2:
        wybrany.nazwa=Boh.p2;
        Boh.p2="0";
        czy=1;
        break;
    case 3:
        wybrany.nazwa=Boh.p3;
        Boh.p3="0";
        czy=1;
        break;
    case 4:
        czy=0;
        break;
    }

    if(czy==1){
    plik="pm_"+wybrany.nazwa+".txt";
    ipmplik.open(plik.c_str());
    ipmplik>>wybrany.atakup>>wybrany.obronaup>>wybrany.hpup>>wybrany.manaup;
    ipmplik.close();

    }
    else{
        wybrany.nazwa="0";
        wybrany.atakup=0;
        wybrany.obronaup=0;
        wybrany.hpup=0;
        wybrany.manaup=0;

    }
    return wybrany;
}
/**
 * @brief wybierzumiejetnosc
 * funkcja pozwalająca na wybranie umijeśtnośći podczas walki
 * @param Boh
 * @return strukturę umijętnosći która zostanie wykorzystana w walce
 */
umiejetnosc wybierzumiejetnosc(postac Boh){
    umiejetnosc wybrana;
    int wyb;
    ifstream iumplik;
    bool czy=0;
    wybrana.manakoszt=0;
    cout<<"wybierz umiejetnosc: "<<endl;
    cout<<"1 "<<Boh.u1<<endl;
    cout<<"2 "<<Boh.u2<<endl;
    cout<<"3 "<<Boh.u3<<endl;
    cin>>wyb;

    switch(wyb){
    case 1:
        wybrana.nazwa=Boh.u1;
        czy=1;
        break;
    case 2:
        wybrana.nazwa=Boh.u2;
        czy=1;
        break;
    case 3:
        wybrana.nazwa=Boh.u3;
        czy=1;
        break;
    case 4:
        czy=0;
        break;
    }

    if(czy==1){
    wybrana.nazwa="um_"+wybrana.nazwa+".txt";
    iumplik.open(wybrana.nazwa.c_str());
    iumplik>>wybrana.nazwa>>wybrana.atakup>>wybrana.obronaup>>wybrana.hpup>>wybrana.manakoszt;
    iumplik.close();
     if(wybrana.manakoszt>Boh.mana){
         cout<<"probojesz uzyc tej umiejetnosci ale masz za malo many i Ci sie nie udaje"<<endl;
     }
    }
    else{
        wybrana.nazwa="0";
        wybrana.atakup=0;
        wybrana.obronaup=0;
        wybrana.hpup=0;
        wybrana.manakoszt=0;

    }
    return wybrana;
}
/**
 * @brief sumujobrazeniapotw
 * funkca sumująca ilość obrażeń zadawanych przez przeciwnika
 * @param przeciwnik
 * @return sumę obrażeń
 */
int sumujobrazeniapotw(potwor przeciwnik){
    srand (time(0));
    int los=1+rand()%(przeciwnik.atak);
    cout<<"przeciwnik atakuej za  "<<los<<endl;
    return los;
}
/**
 * @brief sumujobrazeniaboh
 * funkja sumująca obrażenia zadawane przez bohatera,wlicza użyte umiejętności i przedmioty użyte
 * @param Boh
 * @param um
 * @param pm
 * \see wybierzumiejetnosc
 * \see wybierzprzedmiot
 * @return sumę obrażeń bohatera(Boh.atak+um.atakup+pm.atakup)
 */
int sumujobrazeniaboh(postac Boh, umiejetnosc um, przedmiot pm){
    srand (time(0));
    int suma=Boh.atak+um.atakup+pm.atakup;
    int los=1+rand()%(suma);
    cout<<"atakujesz za "<<los<<endl;
    return los;


}
/**
 * @brief obronaboh
 * funkcja zliczhjaca punkty obrony bohatera
 * bierze pod uwage umiejetnosc, przedmiot oraz punkty obrony bohatera
 * @param Boh
 * @param um
 * @param pm
 * @return liczba losowa w z przedziału [1,suma obrony]
 */
int obronaboh(postac Boh, umiejetnosc um, przedmiot pm){
    srand (time(0));
    int suma=Boh.obrona+um.obronaup+pm.obronaup;
    int los=1+rand()%(suma);
    cout<<"bronisz sie za "<<los<<endl;
    return los;

}
/**
 * @brief obronapotw
 * funkca sumuja obrone przecinwika
 * @param przeciwnik
 * @return liczba losowa z [1,obronaMAXprzecinika]
 */
int obronapotw(potwor przeciwnik){
    srand (time(0));
    int los=1+rand()%(przeciwnik.obrona);
    cout<<"przeciwnik broni sie za "<<los<<endl;
    return los;
}
/**
 * @brief leczenie
 * funkcja korzystajaca z umijetnosci leczenia lub miksury zycia podwyzajaca ilosc punktow zycia bohatera
 * @param boh
 * @param um
 * @param pm
 * @return liczba punktow zycia bohatera powiekszona o wartosc leczenia
 */
postac leczenie(postac boh, umiejetnosc um, przedmiot pm){
   if(um.hpup>0){
       cout<<"uzyawasz umiejetnosci "<<um.nazwa<<"leczysz sobie "<<um.hpup<<" pkt zycia"<<'\n';
      }
   if(pm.hpup){
       cout<<"uzyawasz przedmiotu "<<pm.nazwa<<"leczysz sobie "<<pm.hpup<<" pkt zycia"<<'\n';
   }
    if((boh.hp+um.hpup+pm.hpup)<=(15*boh.lvl)){
       boh.hp=boh.hp+um.hpup+pm.hpup;
      }
   else {
       boh.hp=1.5*boh.lvl;

   }
    um.hpup=0;
    pm.hpup=0;
 return boh;

}
/**
 * @brief podnisieniemany
 * funkjca podwyzajca ilosc punktow many bohatera po uzyciu eliskiru many
 * @param boh
 * @param pm
 * @return punkty many podniesione o wartość punktów podniesionych przez eliksir
 */
postac podnisieniemany(postac boh,  przedmiot pm){
    if(pm.manaup){
        cout<<"uzyawasz przedmiotu "<<pm.nazwa<<" leczysz sobie "<<pm.manaup<<" pkt many"<<'\n';
    }
    boh.mana=+pm.manaup;
    pm.manaup=0;
    return boh;

}
/**
 * @brief walka2
 * funkcja przeprowadzajaca proces walki złożony z wybierania przedmiotów, umiejętności, sumowania i zadawania obrażeń
 * @param przeciwnik
 * @param boh
 * \see wybierzprzedmiot
 * \see wybierzumiejetnosc
 * \see podnisieniemany
 * \see leczenie
 * \see sumujobrazeniaboh
 * \see sumujobrazeniapotw
 * \see obronaboh
 * \see obronapotw
 * \see losujprzedmiot
 * \see levelup
 * @return strukturę postaci po zakończeniu walki bezpośredniej
  */
postac walka2(potwor przeciwnik, postac boh){
   int obrazeniaboh;
   int obrazeniapotw;
   string wyb;
   umiejetnosc um;
   przedmiot pm;
   um.nazwa="0";
   um.atakup=0;
   um.obronaup=0;
   um.hpup=0;
   um.manakoszt=0;
   pm.atakup=0;
   pm.hpup=0;
   pm.manaup=0;
   pm.obronaup=0;


    while(przeciwnik.hp>0&&boh.hp>0){
        system("cls");
        cout<<"walczysz z "<<przeciwnik.nazwa<<endl;
        cout <<"TY:"<<'\n'<<" hp "<<'\t'<<boh.hp<<'\n'<<" atak "<<'\t'<<boh.atak<<'\n'<<" obrona "<<boh.obrona<<'\n'<<" mana "<<'\t'<<boh.mana<<'\n'<<endl;
        cout<<"Przciwnik:"<<endl<<" hp "<<'\t'<<przeciwnik.hp<<'\n'<<" atak "<<'\t'<<przeciwnik.atak<<'\n'<<" obrona "<<przeciwnik.obrona<<'\n'<<endl<<endl;
        if(boh.hp>0){
           cout<<"czy chcesz wybrac umiejetnosc? t/n"<<endl;
           cin>>wyb;
           if(wyb=="t"){
               um=wybierzumiejetnosc(boh);
               boh.mana=boh.mana-um.manakoszt;
               um.manakoszt=0;
               cout<<"uzycie umiejetnsoci kosztowalo Cie "<< um.manakoszt<<" pkt many"<<'\n';
           }

           cout<<"czy chcesz wybrac przedmiot? t/n"<<endl;
            cin>>wyb;
            if(wyb=="t"){
                pm=wybierzprzedmiot(boh);
                cout<<"uzyles "<<pm.nazwa<<'\n';

            }
          boh=leczenie(boh,um, pm);
           boh=podnisieniemany(boh, pm);
           obrazeniaboh=sumujobrazeniaboh(boh,um,pm)-obronapotw(przeciwnik);
            if(obrazeniaboh>0){
           cout<<"zadales "<<obrazeniaboh<<" obrazen"<<endl<<endl;
           przeciwnik.hp=przeciwnik.hp-obrazeniaboh;
            }
            else{
                cout<<"nie zadajesz obrazen"<<endl<<endl;
            }

        }
        if(przeciwnik.hp>0){
            obrazeniapotw=sumujobrazeniapotw(przeciwnik)-obronaboh(boh,um,pm);
            if(obrazeniapotw>0){
            cout<<"przeciwnik zadaje "<<obrazeniapotw<<" obrazen"<<endl<<endl;
            boh.hp=boh.hp-obrazeniapotw;
            }
            else
                cout<<"nie otrzymujesz obrazen"<<endl<<endl;

        }
        else {
            cout<<"przecwinik zginal"<<endl;
        }

       system("PAUSE");



 }
  if (boh.hp<=0){

      cout<<boh.hp;
      cout<<"GAME OVER"<<endl;
      system("PAUSE");

  }
 else{
      system ("cls");
      cout<<"wygrales pojedynek"<<endl;
      boh.exp=boh.exp+przeciwnik.expp;
      cout<<"otrzymujesz "<<przeciwnik.expp<<"punktow exp"<<endl;
      cout<<"masz "<<boh.exp<<"punktow exp"<<endl<<'\n';
      boh=levelup(boh);
      system("PAUSE");
      system ("cls");
      boh=losujprzedmiot(przeciwnik.nazwa,boh);
  }
  return boh;
}
/**
 * @brief walka
 * funkcja przeprowadzająca proces walki od losowania przeciwnika, a potem zakończeniu walki i powrtou do menu
 * @param boh
 * @param wyborlok
 * \see walka2
 * @return strukturę postaci po przeprowadzaniu walki

 */
postac walka(postac boh,  string wyborlok){
    srand (time(0));
    int los;
    ifstream ipotwplik;
    ifstream ilokplik;
    string nazwa;
    potwor przeciwnik;
    ilokplik.open(wyborlok.c_str());





    los=rand()%(3);
       cout<<los<<endl;
       do {
        getline (ilokplik, nazwa);

    }while(nazwa!="potwory:");
       for(int i=0; i<los+1; i++){
            getline(ilokplik, nazwa);

        }
    cout<<"Twoj przeciwnik to: "+nazwa<<endl;


    nazwa="potw_"+nazwa+".txt";
    ipotwplik.open(nazwa.c_str());
    ipotwplik>>przeciwnik.nazwa>>przeciwnik.hp>>przeciwnik.atak>>przeciwnik.obrona>>przeciwnik.expp;
    ipotwplik.close();
    cout <<"TY"<<endl<<"hp "<<boh.hp<<" atak "<<boh.atak<<" obrona "<<boh.obrona<<endl;
    cout<<"przciwnik"<<endl<<"hp "<<przeciwnik.hp<<" atak "<<przeciwnik.atak<<" obrona "<<przeciwnik.obrona<<endl;

   boh=walka2(przeciwnik, boh);


return boh;
}




















