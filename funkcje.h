/**
 *\ file funkcje.h
 * \brief Plik nagłówkowy modułu funkcji
 */




#ifndef FUNKCJE_H
#define FUNKCJE_H
#include <string>
using namespace std;
/**
 * @brief The umiejetnosc struct
 * struktura umijetności np. ataku lub lczenia
 */
struct umiejetnosc{
    ///nazwa umiejętnosći
    string nazwa;
    ///wspóczynnik określajy liczbę punktów podnoszących atak
    int atakup;
    ///wspóczynnik określajy liczbę punktów podnoszących obronę
    int obronaup;
   ///wspóczynnik określajy liczbę punktów podnoszących życie
    int hpup;
   ///wspóczynnik określajy liczbę punktów many wydanych na umiejętność
    int manakoszt;

};
/**
 * @brief The przedmiot struct
 * struktura przedmiotu np. kija lub elisiru many
 */
struct przedmiot{
    ///nazwa umiejętnosći
    string nazwa;
     ///wspóczynnik określajy liczbę punktów podnoszących atak
    int atakup;
    ///wspóczynnik określajy liczbę punktów podnoszących obronę
    int obronaup;
    ///wspóczynnik określajy liczbę punktów podnoszących życie
    int hpup;
    ///wspóczynnik określajy liczbę punktów podnoszących manę
    int manaup;
    };


/**
 * @brief The postac struct
 * struktura bohatera, wykorzystywana do więkoszości działań w grze
 */
struct postac{
    ///nick bohatera
    string nazwa;
    ///liczba punktów okrelślących punkty życia bohatera
    int hp;
    ///liczba punktów okrelślących punkty many bohatera
    int mana;
    ///liczba okrelśląca maksymalny atak bohatera
    int atak;
     ///liczba okrelśląca maksymalną obronę bohatera
    int obrona;
     ///liczba okrelśląca ilość punktów doświadczenia bohatera
    int exp;
    /// licza określająca aktulany level bohatera
    int lvl;
    ///liczba określąca ilość złota
    int gold;

    ///nazwy umiejśtności boahtera
    /// wykorzystana do otwarcia pliku ze strukturą umijętności
    string u1;
    string u2;
    string u3;

    ///nazwy przedmiotów boahtera
    /// wykorzystana do otwarcia pliku ze strukturą przedmiotu
     string p1;
    string p2;
    string p3;
    string p4;
    string p5;



};

/**
 * @brief The potwor struct
 * struktura przeciwnika wykorzystywna w czasie walki
 */
struct potwor{
    ///nazwa przeciwnika
    string nazwa;
    /// punkty życia przeciwnika
    int hp;
    ///liczba określająca maksymalny atak przeciwnika
    int atak;
    /// liczba określąjca maksymlną obronę przeciwnika
    int obrona;
    /// liczba określająca liczbe punktów dośiwadcznia które otrzyma bohater po poknaniu przciwnika
    int expp;
    ///nawzwy przedmiotów, które może wydropić przeciwik po przegranej
    string drop;


};

int obronapotw(potwor przeciwnik);
int obronaboh(postac Boh, umiejetnosc um, przedmiot pm);
void wyswietlpostac(postac Bohater);
postac tworzeniepostaci(string nick);
postac wczytajpostac(string nick);
void MenuStartowe();
void MenuGry();
void Zapisz(postac Bohater);
postac levelup(postac Bohater);
przedmiot wybierzprzedmiot(postac &Boh);
umiejetnosc wybierzumiejetnosc(postac Boh);
postac walka(postac boh, string wyborlok);
void gra(postac Bohater);
postac walka2(potwor przeciwnik, postac boh);
int sumujobrazeniaboh(postac Boh, umiejetnosc um, przedmiot pm);
int sumujobrazeniapotw(potwor przeciwnik);
postac losujprzedmiot(string nazwapotwora, postac Boh);
postac leczenie(postac boh, umiejetnosc um, przedmiot pm);
postac nowyprzedmiot(postac Boh, string nazwa);
postac nowaumiejetnosc (postac boh);
postac podnisieniemany(postac boh, przedmiot pm);
#endif // FUNKCJE_H
